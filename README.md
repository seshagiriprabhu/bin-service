# Secular service
Secularism is the main plank for the political parties in India elections 2014. 
Not development, not jobs, not infrastructure, not economy, not even corruption, 
not cyber security, not anything else. Only secularism. We decided to write a 
secular service to find a solution for this.  

## Files given to the participants
1. `server`             - ELF-32, unstripped binary file 
2. `README.md`          - A quick info about the service and blah blah
3. `setup_database.sh`  - A shellscript to setup and create a SQLite DB to store
flags. A file named `service.db` will be created upon executing this script. 

## Requirements
This vulnerable program requires these packages: `sqlite3`, `libsqlite3-dev`

```bash
sudo apt-get install sqlite3 libsqlite3-dev
```
## How to setup the DB

```bash
./setup_database.sh
```

## How to run the server program

```bash
./server
```

## How to connect to the server program

You could use either `telnet` or `netcat` to connect to the server program. 
```bash
nc YOUR_VULN_IMAGE_IP 50000
```
or
```bash
telnet YOUR_VULN_IMAGE_IP 50000
```
Service port: `50000`

