#!/usr/bin/python

# Returns:
# 0: success
# 1: generic error
# 5: service up, no flag  (not used for 'store')
# 9: garbled output
# 13: network

import os
import socket
import string
import sys
import time

service_port = 50000

def usage():
    """
    Defines the usage of this script
    """
    print "Usage:", sys.argv[0], "<operation> <ip> <id> <flag>"

def connect():
    """
    Connects to the service
    """
    try:
        sock.connect((ip, service_port))
        sock.recv(1024)
    except socket.timeout:
        print "Connection timed out"
        exit(13)
    except Exception, e:
        print e
        exit(13)

def store(ip, flag_id, flag):
    """
    Function to store flag to the service
    Successful User regristration implies
    the flag storing in the user db
    """
    connect()

    try:
        # Choose user registration
        sock.send("2\n")
        sock.recv(28)

        # Setting username as flag id
        sock.send(flag_id + "\n")
        sock.recv(28)

        # Setting password as flag
        sock.send(flag + "\n")
        reply = sock.recv(19)

        if reply.strip() != "User Registered!!":
            print "Invalid response"
            exit(1)

        exit(0)

    except socket.timeout:
        print "Connection time out"
        exit(13)

    except Exception, e:
        print e
        exit(1)



def retrieve(ip, flag_id, flag):
    """
    Function to retrieve
    Planted flag can be verified by 
    checking if login was successful
    """

    connect()
    try:
        # login
        sock.send("1\n")
        sock.recv(28)

        # Setting username as flag id
        sock.send(flag_id + "\n")
        sock.recv(28)

        # Setting password as flag
        sock.send(flag + "\n")
        reply = sock.recv(19)

        if reply.strip() != "Login Successful!!":
            print "Invalid response"
            exit(1)

        exit(0)

    except socket.timeout:
        print "Connection time out"
        exit(13)

    except Exception, e:
        print e
        exit(1)


if __name__ == "__main__":
    """
    Main function
    """
    if len(sys.argv) < 5:
        usage()
        exit(1)

    action = sys.argv[1]
    ip = sys.argv[2]
    flag_id = sys.argv[3]
    flag = sys.argv[4]
    socket.setdefaulttimeout(120)
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    if action.lower() == "store":
        store(ip, flag_id, flag)

    elif action.lower() == "retrieve":
        retrieve(ip, flag_id, flag)

    else:
        print "Action has to be either store or retrieve"
        print "Internal gameserver scipt error"
        exit(1)

