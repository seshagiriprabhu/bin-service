import struct
import telnetlib
import socket
import sys
import os

import M2Crypto
import string

ip = '127.0.0.1'
port = 50000

def random(length = 32):
    chars = string.ascii_uppercase + string.digits + string.ascii_lowercase
    password = ''
    for i in range(length):
        password += chars[ord(M2Crypto.m2.rand_bytes(1)) % len(chars)]
    return password

if __name__ == "__main__":
    for i in range(40):
        username = random()
        password = random()
        print username + ":" + password
        #reg_user(username, password)
        #reg_verify(username, password)
        os.system("python gameserver_script.py store localhost " + username + " " +
                password)
        os.system("python gameserver_script.py retrieve localhost " + username + " " +
                password)
