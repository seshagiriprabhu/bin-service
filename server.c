/* Standard libraries */
#define _GNU_SOURCE
#define _XOPEN_SOURCE
#include <netinet/in.h> /* in_addr structure */
#include <netinet/tcp.h>/* tcp connection */
#include <arpa/inet.h>  /* inet_ntoa() to format IP address */
#include <stdio.h>      /* stderr, stdout */
#include <stdlib.h>     /* exit */
#include <unistd.h>     /* read, send, close */
#include <errno.h>      /* error display */
#include <ctype.h>      /* isalnum */
#include <string.h>     /* getline */
#include <sys/types.h>  /* socket options */
#include <sys/socket.h> /* socket */
#include <sqlite3.h>    /* database */
#include <signal.h>     /* signal */
#include <pthread.h>    /* thread */

/* Macros */
typedef int bool;
#define true 1
#define false 0
#define DB "service.db"
#define FLAG_LEN 32
#define PORT 50000
#define LISTEN_QUEUE_LIMIT 20
typedef unsigned int u_int;
typedef unsigned long ulong;

/* Function declaration */
void disconnect_client(int, struct sockaddr_in);
void send_close_client(int, struct sockaddr_in, int);
void recv_close_client(int, struct sockaddr_in, int);
char *strip(const char *);
void read_flag(int, struct sockaddr_in);
void read_flag_2(int, struct sockaddr_in, const char *, const char *);
bool login(int, struct sockaddr_in, socklen_t);
bool reg_user(int, struct sockaddr_in, socklen_t);
bool client_handler(int, struct sockaddr_in, socklen_t);

/* Disconnects a client sock */
void disconnect_client(int clientsock, struct sockaddr_in client_addr) {
    fprintf(stdout, "[+] Client disconnected: %s\n", inet_ntoa(client_addr.sin_addr));
    shutdown(clientsock, SHUT_WR);
    close(clientsock);
}

/* Closes client socket */
void send_close_client(int clientsock, struct sockaddr_in client_addr, int ret_val) {
    if (ret_val == -1) {
        fprintf(stderr, "\033[1;31m[-] Send Socket error - %d \033[0m\n", errno);
    } 
    disconnect_client(clientsock, client_addr);
}

/* Closes client socket */
void recv_close_client(int clientsock, struct sockaddr_in client_addr, int ret_val) {
    if (ret_val == -1) {
        fprintf(stderr, "\033[1;31m[-] Recv Socket error - %d \033[0m\n", errno);
    } 
    disconnect_client(clientsock, client_addr);
}

/* Strips the \r \n and non alpha numericals from a string */
char *strip(const char *string) {
    char *out = malloc(strlen(string) + 1);
    if (out) {
        char *out2 = out;
        while(*string != '\0') {
            /* Skips \r, \n  and  non alpha numeric characters */
            if ((*string != '\r' && \
                        *string != '\n') && \
                    isalnum(*string)) {
                *out2++ = *string++;
            } else {
                ++string;
            }
        }
        *out2 = '\0'; /* Appends a null terminator */
    }
    return out;
}

/* Reads flag without comparing with the input */
void read_flag(int clientsock, struct sockaddr_in client_addr) {
    sqlite3 *db;        /* Database handle */
    const char *sql;    /* SQL statement */
    int nByte = -1;     /* Maximum length of sql statement */
    sqlite3_stmt *res;  /* Statement handle */
    int result = 0;     /* stores the result of recv/send */

    /* Open database */
    if (sqlite3_open(DB, &db) != SQLITE_OK) {
        fprintf(stderr, "\033[1;31m[-] Unable to read file: %s\033[0m\n", DB);
        close(clientsock);
        exit(EXIT_FAILURE);
    }

    /* Create SQL statement */
    sql = "SELECT username, password FROM users;";

    /* Execute SQL Statement */
    if (sqlite3_prepare_v2(db, sql, nByte, &res, NULL) != SQLITE_OK) {
        fprintf(stderr, "\033[1;31m[-] Unable to execute sql statement %s\033[0m\n", sql);
        close(clientsock);
        exit(EXIT_FAILURE);
    }

    /* Sockopt is set as TCP_NODELAY in-order for flushing the buffer immediately without 
    * any loss. This is done for sending important or urgent data  */
    int flag = 1;                                       /* Flag set as one for TCP_NODELAY */
    setsockopt(clientsock, IPPROTO_TCP, TCP_NODELAY, (char *) &flag, sizeof(int));
    /* Evaluating SQL statement */
    while (sqlite3_step(res) == SQLITE_ROW) {
        char *uname = (char *)sqlite3_column_text(res, 0);  /* stores password from DB */
        char *pass  = (char *)sqlite3_column_text(res, 1);  /* stores username from DB */
        u_int len = strlen(uname) + strlen(pass) + 3;       /* length of outbuf */
        char *outBuf = malloc(len);                         /* stores the pass:uname */
        /* Write the username:Password to Outbuf */
        snprintf(outBuf, len, "%s:%s\n", uname, pass);
        /* Send the username:password to attacker */
        if ((result = sendto(clientsock, outBuf, len, MSG_NOSIGNAL, 
                    (struct sockaddr *)&client_addr, sizeof(client_addr))) < 1) { 
            send_close_client(clientsock, client_addr, result);
        }
        free(outBuf);
    }
    /* Writing out the outbuf for the participants who hosted this service to make them 
     * understand that they have been attacked by someone */
    fprintf(stdout, "You have been attacked by: %s\n", inet_ntoa(client_addr.sin_addr));
    /* Sockopt is reset back to send non urgent/important data */
    flag = 0; 
    setsockopt(clientsock, IPPROTO_TCP, TCP_NODELAY, (char *) &flag, sizeof(int));

    sqlite3_finalize(res);
    sqlite3_close(db);
}

/* Reads flag comparing with the input */
void read_flag_2(int clientsock, struct sockaddr_in client_addr, const char *username, const char *password) {
    sqlite3 *db;        /* Database handle */
    char *sql;          /* SQL statement */
    int nByte = -1;     /* Maximum length of sql statement */
    sqlite3_stmt *res;  /* Statement handle */
    bool found = false; /* flag to check if the user details exits in db */
    int ret_val = 0;    /* stores the return value from send and recv */

    /* Open database */
    if (sqlite3_open(DB, &db) != SQLITE_OK) {
        fprintf(stderr, "\033[1;31m[-] Unable to read file: %s\033[0m\n", DB);
        close(clientsock);
        exit(EXIT_FAILURE);
    }

    /* Create SQL statement */
    u_int len = strlen(username) + 60;
    sql = malloc(len);
    snprintf(sql, len-1, "SELECT username, password FROM users WHERE " \
            "username=\'%s\';", username);
    /* fprintf(stdout, "SQL: %s\n", sql); */

    /* Execute SQL Statement */
    if (sqlite3_prepare_v2(db, sql, nByte, &res, NULL) != SQLITE_OK) {
        fprintf(stderr, "\033[1;31m[-] Unable to execute sql statement %s\033[0m\n", sql);
        close(clientsock);
        exit(EXIT_FAILURE);
    }

    /* Evaluating SQL statement */
    while (sqlite3_step(res) == SQLITE_ROW) {
        /* Stores the username retreived from DB */
        char *uname = (char *)sqlite3_column_text(res, 0); 
        /* Stores the password retreived from DB */
        char *pass = (char *)sqlite3_column_text(res, 1);
        /* Checks if the user given password and one retreived from DB are same */
        if (strncmp(password, pass, strlen(pass)) == 0) {
            /* Length = FLAG_LEN + FLAG_LEN + Column = 1 + \n\0 = 2 = 65 */
            len = FLAG_LEN * 2 + 3;
            /* Stores the buffer for printing the usernmae:password */
            char *outBuf = malloc(len);
            snprintf(outBuf, len, "%s:%s\n", uname, pass);
            /*send(clientsock, outBuf, len, MSG_NOSIGNAL);*/
            free(outBuf);
            found = true;
            break;
        }
    }

    /* If none of username:password combo matches */
    if (found == false) {
        if ((ret_val = sendto(clientsock, "Invalid username or password!!\n", 31, 
                    MSG_NOSIGNAL, (struct sockaddr *)&client_addr, sizeof(client_addr))) < 1) {
            send_close_client(clientsock, client_addr, ret_val);
        }
    } else { /* If password match found */
        if ((ret_val = sendto(clientsock, "Login Successful!!\n", 19, MSG_NOSIGNAL, 
                    (struct sockaddr *)&client_addr, sizeof(client_addr))) < 1) {
            send_close_client(clientsock, client_addr, ret_val);
        }
    }

    sqlite3_finalize(res);
    sqlite3_close(db);
}

/* Handles client login */
bool login(int clientsock, struct sockaddr_in client_addr, socklen_t clientlen) {
    int ret_val = 0;                        /* stores the return values from send and recv */
    char *username = malloc(FLAG_LEN + 1);  /* stores the username */
    int v1 = 0x564f5445;                    /* Junk variable - convert to char to see magic */
    int v2 = 0x464f52;                      /* Junk variable - convert to char to see magic */
    int v3 = 0x424a50;                      /* Junk variable - convert to char to see magic */
    int check = 0;                          /* Check ~ vuln variable */
    char password[FLAG_LEN + 1];            /* stores the password */

    /* Requesting usernmae */
    if ((ret_val = sendto(clientsock, "Please enter your username: ", 28, 
                MSG_NOSIGNAL, (struct sockaddr *)&client_addr, sizeof(client_addr))) < 1) {
        send_close_client(clientsock, client_addr, ret_val);
        return false;
    }

    /* Set username content as null */
    memset(username, '\0', FLAG_LEN + 1);

    /* Receive usernmae */
    if ((ret_val = recvfrom(clientsock, username, FLAG_LEN + 1, 
                    MSG_CMSG_CLOEXEC, (struct sockaddr *)&client_addr, &clientlen)) < 1) {
        recv_close_client(clientsock, client_addr, ret_val);
        return false;
    }

    /* Checks if username length > FLAG_LEN  */
    if (ret_val > FLAG_LEN + 1) {
        /* Sends if the username length is greater than 33 */
        if ((ret_val = sendto(clientsock, "\033[1;31m[-] Invalid username length\033[0m\n", 40, 
                    MSG_NOSIGNAL, (struct sockaddr *)&client_addr, sizeof(client_addr))) < 1) {
            disconnect_client(clientsock, client_addr); 
            return false;
        }
        disconnect_client(clientsock, client_addr); 
        return false;
    }
    memset(username+FLAG_LEN, '\0', 1);

    /* Just for shit */
    v3 = v1 + (v2*v1) + ((v2/v1)-v1);
    v2 = v3 - 1;

    /* Request password */
    if ((ret_val = sendto(clientsock, "Please enter your password: ", 28, 
                MSG_NOSIGNAL, (struct sockaddr *)&client_addr, sizeof(client_addr))) < 1) {
        send_close_client(clientsock, client_addr, ret_val);
        return false;
    }

    /* Set password content as null */
    memset(password, '\0', FLAG_LEN + 1);

    /* Receive password */
    if ((ret_val = recvfrom(clientsock, password, 37, MSG_CMSG_CLOEXEC,
                     (struct sockaddr *)&client_addr, &clientlen)) < 1) {
        recv_close_client(clientsock, client_addr, ret_val);
        return false;
    }
    
    if (ret_val > 0 && ret_val < 38) {
        /* If password was overflown and check is overwritten as 0x6f4d614e */
        if (check == 0x6f4d614e) {
            /* Function which doesn't compare username password and prints everything */
            read_flag(clientsock, client_addr);
        } else {
            /* Function which checks given username:password for validating login */
            read_flag_2(clientsock, client_addr, strip(username), strip(password));
        }
    } else {
        /* Disconnects if the password length is greater than 33 */
        if ((ret_val = sendto(clientsock, "\033[1;31mInvalid password\033[0m\n", 21, 
                    MSG_NOSIGNAL, (struct sockaddr *)&client_addr, sizeof(client_addr))) < 1) { 
            send_close_client(clientsock, client_addr, ret_val);
            return false;
        }
    }
    return true;
}

/* Handles user registration */
bool reg_user (int clientsock, struct sockaddr_in client_addr, socklen_t clientlen) {
    char *username = malloc(FLAG_LEN + 1);  /* Stores the username */
    char *password = malloc(FLAG_LEN + 1);  /* Stores the password */

    int uname_len = 0;      /* Stores the length of username */
    int pass_len = 0;       /* Stores the length of password */

    sqlite3 *db;            /* Database handle */
    char *sql;              /* SQL statement */
    
    int ret_val = 0;        /* stores the ret val of recv/send func */

    /* Open database */
    if (sqlite3_open(DB, &db) != SQLITE_OK) {
        fprintf(stderr, "\033[1;31m[-] Unable to read file: %s\033[0m\n", DB);
        close(clientsock);
        exit(EXIT_FAILURE);
    }

    /* Request username */
    if ((ret_val = sendto(clientsock, "Please enter your username: ", 28, MSG_NOSIGNAL, 
                    (struct sockaddr *)&client_addr, sizeof(client_addr))) < 1) {
        send_close_client(clientsock, client_addr, ret_val);
        return false;
    }

    /* Setting username content as NULL */
    memset(username, '\0', FLAG_LEN + 1);

    /* Receive username */
    uname_len = recvfrom(clientsock, username, FLAG_LEN + 1, MSG_CMSG_CLOEXEC, 
            (struct sockaddr *)&client_addr, &clientlen);

    /* Socket error or client disconnects */
    if (uname_len < 1) {
        recv_close_client(clientsock, client_addr, uname_len);
        return false;
    }

    /* Checks if input length > FLAG_LEN */
    if (uname_len > FLAG_LEN + 1) {
        /* Disconnects if the input length is greater than 33 */
        if ((ret_val = sendto(clientsock, "\033[1;31m[-] Invalid username length\033[0m\n", 40, 
                    MSG_NOSIGNAL, (struct sockaddr *)&client_addr, sizeof(client_addr))) < 1) {
            disconnect_client(clientsock, client_addr); 
            return false;
        }
        disconnect_client(clientsock, client_addr); 
        return false;
    }

    /* Setting the last bit of username as NULL */
    if (uname_len == FLAG_LEN + 1) {
        memset(username+FLAG_LEN, '\0', 1);
    } else { /* set all the uname_len+ bytes to zero */
        memset(username+uname_len, '\0', (FLAG_LEN + 1) - uname_len);
    }

    /* Read the password from user */
    if ((ret_val = sendto(clientsock, "Please enter your password: ", 28, MSG_NOSIGNAL, 
                (struct sockaddr *)&client_addr, sizeof(client_addr))) < 1) {
        send_close_client(clientsock, client_addr, ret_val);
        return false;
    }

    /* Setting password content as NULL */
    memset(password, '\0', FLAG_LEN + 1);

    /* Receive password */
    pass_len = recvfrom(clientsock, password, FLAG_LEN + 1, MSG_CMSG_CLOEXEC, 
            (struct sockaddr *)&client_addr, &clientlen);

    /* Socket error or client disconnects */
    if (pass_len < 1) {
        recv_close_client(clientsock, client_addr, pass_len);
        return false;
    }

    /* Checks if input length > FLAG_LEN */
    if (pass_len > FLAG_LEN + 1) {
        if ((ret_val = sendto(clientsock, "\033[1;31m[-] Invalid password length\033[0m\n", 40, 
                    MSG_NOSIGNAL, (struct sockaddr *)&client_addr, sizeof(client_addr))) < 1) {
            disconnect_client(clientsock, client_addr); 
            return false;
        }
        disconnect_client(clientsock, client_addr); 
        return false;
    }

    /* Setting the last bit of password as NULL */
    if (pass_len == FLAG_LEN + 1) {
        memset(password+FLAG_LEN, '\0', 1);
    } else { /* set all the pass_len+ bytes to zero */
        memset(password+pass_len, '\0', (FLAG_LEN + 1) - pass_len);
    }

    /* create SQL statement */
    char *uname = strip(username);
    char *pass  = strip(password);
    u_int len = strlen(uname) + strlen(pass) + 60;
    sql = malloc(len);
    snprintf(sql, len, "INSERT INTO users (username, password) " \
            "VALUES (\'%s\', \'%s\');", uname, pass);
    /* fprintf(stdout, "sql: %s\n", sql); */

    /* Execute SQL statement */
    if (sqlite3_exec(db, sql, 0, 0, 0) != SQLITE_OK) {
        fprintf(stderr, "\033[1;31m[-] SQL error\033[0m\n");
        return false;
    } else {
        if ((ret_val = sendto(clientsock, "User Registered!!\n", 18, MSG_NOSIGNAL, 
                    (struct sockaddr*)&client_addr, sizeof(client_addr))) < 1) {
            send_close_client(clientsock, client_addr, ret_val);
            return false;
        } else {
            return true;
        }
    }
    sqlite3_close(db);
    return false;
}

bool client_handler(int clientsock, struct sockaddr_in client_addr, socklen_t clientlen) {
    char input[4];          /* Stores the user choice in string format */
    uint32_t choice;        /* Stores the user choice in int format */
    int ret_val = 0;        /* result of recv/send */
    char menu[] =   
        "\n#########################################\n"
        "#\tWelcome to secular service\t#\n"
        "#########################################\n"
        "1. Login\n"
        "2. Register\n"
        "3. Exit\n"
        "Enter your choice: ";

    do {
        /* Ignores all the SIGPIPE signals */
        signal(SIGPIPE, SIG_IGN);


        /* Send menu */
        if ((ret_val = sendto(clientsock, &menu, sizeof(menu), MSG_NOSIGNAL, 
                    (struct sockaddr *)&client_addr, sizeof(client_addr))) < 1) { 
            send_close_client(clientsock, client_addr, ret_val);
            return false;
        }

        /* Receive choice from user in char format */
        if ((ret_val = recvfrom(clientsock, input, sizeof(input), MSG_CMSG_CLOEXEC, 
                        (struct sockaddr *)&client_addr, &clientlen)) < 1) {
                recv_close_client(clientsock, client_addr, ret_val);
                return false;
        }
        /* Convert char to int */
        choice = atoi(input);

        /* Process choice */
        if (choice == 1) {          /* Login */
            if (true == login(clientsock, client_addr, clientlen)) {
                return true;
            }
        } else if (choice == 2) {   /* Register */
            if (true == reg_user(clientsock, client_addr, clientlen)) {
                return true;
            } 
        } else if (choice == 3) {   /* Exit */
            disconnect_client(clientsock, client_addr); 
            return false;
        } else {                    /* Invalid choice */
            if ((ret_val = sendto(clientsock, "Invalid choice\n", 15, MSG_NOSIGNAL, 
                        (struct sockaddr *)&client_addr, sizeof(client_addr))) < 1) {
                send_close_client(clientsock, client_addr, ret_val);
                return false;
            } 
        }
    } while (choice > 0 && choice < 4);
    return true;
}

int main() 
{
    int serversock = 0;     /* Stores the server socket descriptor */
    struct sockaddr_in serv_addr;      /* Server information */
    
    int optval = 1;         /* For socket options */

    int keepcnt = 5;        /* maximum number of keepalive probes */
    int keepidle = 30;      /* The time the connection needs to remain idle */
    int keepintvl = 120;    /* The time between individual keepalive probes */
    

    /* Create the TCP socket */
    if ((serversock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP)) < 0) {
        fprintf(stderr, "\033[1;31m[-] Failed to create socket\033[0m\n");
        close(serversock);
        exit(EXIT_FAILURE);
    }

    /* Construct the server sockaddr_in structure */
    memset(&serv_addr, '0', sizeof(serv_addr));     /* Clear struct */
    serv_addr.sin_family = AF_INET;                 /* Internet/IP  */
    serv_addr.sin_addr.s_addr = htonl(INADDR_ANY);  /* IP address   */
    serv_addr.sin_port = htons(PORT);               /* Server port  */

    setsockopt(serversock, SOL_SOCKET, SO_KEEPALIVE, (void *)&optval, sizeof(int));
    setsockopt(serversock, IPPROTO_TCP, TCP_KEEPCNT, &keepcnt, sizeof(int));
    setsockopt(serversock, IPPROTO_TCP, TCP_KEEPIDLE, &keepidle, sizeof(int));
    setsockopt(serversock, IPPROTO_TCP, TCP_KEEPINTVL, &keepintvl, sizeof(int));

    /* Bind the server socket */
    if (bind(serversock, (struct sockaddr*)&serv_addr, sizeof(serv_addr)) == -1) {
        fprintf(stderr, "\033[1;31m[-] Bind failed\033[0m\n");
        close(serversock);
        exit(EXIT_FAILURE);
    }

    /* Listen on the server socket */
    if (listen(serversock, LISTEN_QUEUE_LIMIT) < 0) {
        fprintf(stderr, "\033[1;31m[-] listen failed\033[0m\n");
        close(serversock);
        exit(EXIT_FAILURE);
    } 

    /* Read until cancelled */
    while(1)
    {   
        struct sockaddr_in client_addr;     /* Client information */
        socklen_t clientlen;                /* stores the length of client address */
        int clientsock = 0;                 /* Stores the client socket descriptor */

        if ((clientsock = accept(serversock, (struct sockaddr*) &client_addr, &clientlen)) < 0){
            fprintf(stderr, "\033[1;31m[-] Failed to accept connection from %s\033[0m\n", inet_ntoa(client_addr.sin_addr));
            close(clientsock);
        } else {
            fprintf(stdout, "[+] Client connected: %s\n", inet_ntoa(client_addr.sin_addr));
        }
        if (true == client_handler(clientsock, client_addr, clientlen)) {
            fprintf(stdout, "[+] Client disconnected: %s\n", inet_ntoa(client_addr.sin_addr));
            close(clientsock);
        }
    }
    close(serversock);
}
